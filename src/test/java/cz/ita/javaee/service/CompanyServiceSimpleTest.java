package cz.ita.javaee.service;

import cz.ita.javaee.service.api.CompanyService;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class CompanyServiceSimpleTest {

    private CompanyService companyService = new CompanyServiceInMemory();

    @Test
    public void testFindAll() {
        Assert.assertEquals(2, companyService.findAll().size());
    }
}
