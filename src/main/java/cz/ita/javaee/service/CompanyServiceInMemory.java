package cz.ita.javaee.service;

import cz.ita.javaee.dto.AddressDto;
import cz.ita.javaee.dto.CompanyDto;
import cz.ita.javaee.service.api.CompanyService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class CompanyServiceInMemory implements CompanyService {
    private final List<CompanyDto> DATA;

    {
        DATA = new ArrayList<>();
        CompanyDto company1 = new CompanyDto();
        company1.setId(1L);
        company1.setName("Best soft s.r.o.");
        company1.setCompanyId("11223344");
        company1.setVatId("CY11223344");
        company1.setActive(true);
        company1.setOwnershipped(true);
        company1.setNote("Hlavní firma.");
        company1.setCreated(LocalDateTime.now());
        AddressDto addressCompany1 = new AddressDto();
        addressCompany1.setStreetName("Lidická");
        addressCompany1.setZipCode("60200");
        addressCompany1.setCountry("Brno");
        addressCompany1.setHouseNumber("12");
        addressCompany1.setCountry("CZE");
        company1.setAddress(addressCompany1);

        CompanyDto company2 = new CompanyDto();
        company2.setId(2L);
        company2.setName("Wekolo");
        company2.setCompanyId("11112222");
        company2.setVatId("SR11112222");
        company2.setActive(true);
        company2.setOwnershipped(false);
        company2.setCreated(LocalDateTime.now());
        AddressDto addressCompany2 = new AddressDto();
        addressCompany2.setStreetName("Farska");
        addressCompany2.setZipCode("01898");
        addressCompany2.setCountry("Stropkov");
        addressCompany2.setHouseNumber("1/a");
        addressCompany2.setCountry("SVK");
        company2.setAddress(addressCompany2);

        DATA.add(company1);
        DATA.add(company2);
    }


    @Override
    public List<CompanyDto> findAll() {
        return DATA;
    }

    @Override
    public CompanyDto create(CompanyDto company) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public CompanyDto update(CompanyDto company) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public void delete(Long id) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
}
