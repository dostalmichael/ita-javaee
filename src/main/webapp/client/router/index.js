import Vue from 'vue'
import Router from 'vue-router'
import companiesView from 'views/companies'
import employeesView from 'views/employees'
import tmProjectsView from 'views/tmProjects'
import overviewView from 'views/overview'

Vue.use(Router);

const router = new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      redirect: '/overview'
    },
    {
      path: '/overview',
      component: overviewView
    },
    {
      path: '/companies',
      component: companiesView,
      meta: {
        requiresLoggedIn: true,
        requiresRole: 'ADMIN'
      }
    },
    {
      path: '/employees',
      component: employeesView,
      meta: {
        requiresLoggedIn: true,
        requiresRole: 'ADMIN'
      }
    },
    {
      path: '/tm-projects',
      component: tmProjectsView,
      meta: {
        requiresLoggedIn: true,
        requiresRole: 'DEALER'
      }
    },
    {
      path: '/*',
      redirect: '/'
    }
  ]
});

export default router;

