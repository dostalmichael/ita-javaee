'use strict';

import Vue from "vue";
import VeeValidate from "vee-validate";
import skDictionary from "./sk";
import decimalValidator from "./decimalValidator";
import minValueValidator from "./minValueValidator";

export default () => {

  const config = {
    locale: 'sk',
    dictionary: {
      sk: {
        messages: skDictionary
      }
    }
  };

  Vue.use(VeeValidate, config);
}
