import actions from './employerActions';
import mutations from './employerMutations';

const state = {
  items: []
};

export default {
  namespaced: true,
  state,
  mutations: mutations,
  actions: actions
}
