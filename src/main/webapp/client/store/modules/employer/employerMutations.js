import { EMPLOYER_LIST, EMPLOYER_ACTIVATE, EMPLOYER_DEACTIVATE } from '../../mutationTypes'
import _ from 'lodash'

const mutations = {
  [EMPLOYER_LIST](state, action){
    state.items = action.items
  },
  [EMPLOYER_ACTIVATE](state, id){
    _.find(state.items, {id: id}).active = true;
  },
  [EMPLOYER_DEACTIVATE](state, id){
    _.find(state.items, {id: id}).active = false;
  }
};

export default mutations;
