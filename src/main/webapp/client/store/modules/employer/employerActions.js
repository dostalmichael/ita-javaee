import * as types from "../../mutationTypes";
import employerService from "../../../services/employerService";
import _ from 'lodash';

const actions = {
  async getAll ({ state, dispatch, commit}, force) {
    if (!state.items || state.items.length === 0 || force) {
      dispatch('app/loadingDataEnable', null, {root: true});
      const employees = await employerService.findAll();
      await commit(types.EMPLOYER_LIST, {
        items: employees
      });
      dispatch('app/loadingDataDisable', null, {root: true});
    }
  },
  async create ({ dispatch, commit}, employer) {
    try {
      let response = await employerService.create(employer);
      if (response.ok) {
        await dispatch('getAll', true);
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async update ({ dispatch, commit}, employer) {
    try {
      let response = await employerService.update(employer);
      if (response.ok) {
        await dispatch('getAll', true);
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async activate ({ state, commit}, id) {
    try {
      await commit(types.EMPLOYER_ACTIVATE, id);
      let employer = _.find(state.items, { id: id});
      let response = await employerService.update(employer);
      return !!response.ok;
    } catch (ex) {
      console.error(ex);
    }
  },
  async deactivate ({ state, commit}, id) {
    try {
      await commit(types.EMPLOYER_DEACTIVATE, id);
      let employer = _.find(state.items, { id: id});
      let response = await employerService.update(employer);
      return !!response.ok;
    } catch (ex) {
      console.error(ex);
    }
  },
  async delete ({dispatch, commit}, id) {
    let response = await employerService.delete(id);
    if (response.ok) {
      dispatch('getAll', true);
      return true;
    } else {
      return false;
    }
  }
};

export default actions
