import {APP_CONFIGURATION_LOAD, APP_SUBMIT_PROTECTION_ENABLE, APP_SUBMIT_PROTECTION_DISABLE, APP_LOADING_DATA_ENABLE, APP_LOADING_DATA_DISABLE} from 'store/mutationTypes';
import configurationService from 'services/configurationService';

export default {
  async protect ({commit}) {
    await commit(APP_SUBMIT_PROTECTION_ENABLE);
  },
  async unprotect ({commit}) {
    await commit(APP_SUBMIT_PROTECTION_DISABLE);
  },
  async loadingDataEnable ({commit}) {
    await commit(APP_LOADING_DATA_ENABLE);
  },
  async loadingDataDisable ({commit}) {
    await commit(APP_LOADING_DATA_DISABLE);
  },
  async loadConfiguration ({commit, state}) {
    if (!state.configuration) {
      const configuration = await configurationService.getConfiguration();
      await commit(APP_CONFIGURATION_LOAD, {
        configuration: configuration
      });
    }
  }
}
