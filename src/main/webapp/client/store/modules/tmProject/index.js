import actions from './tmProjectActions';
import mutations from './tmProjectMutations';

const state = {
  items: []
};

export default {
  namespaced: true,
  state,
  mutations: mutations,
  actions: actions
}
