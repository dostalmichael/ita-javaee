import { TM_PROJECT_LIST } from '../../mutationTypes'

const mutations = {
  [TM_PROJECT_LIST](state, action){
    state.items = action.items
  }
};

export default mutations;
