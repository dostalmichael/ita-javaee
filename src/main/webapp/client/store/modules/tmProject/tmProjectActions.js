import * as types from "../../mutationTypes";
import tmProjectService from "../../../services/tmProjectService";

const actions = {
  async getAll ({ state, dispatch, commit}, force) {
    if (!state.items || state.items.length === 0 || force) {
      dispatch('app/loadingDataEnable', null, {root: true});
      const tmProjects = await tmProjectService.findAll();
      await commit(types.TM_PROJECT_LIST, {
        items: tmProjects
      });
      dispatch('app/loadingDataDisable', null, {root: true});
    }
  },
  async create ({ dispatch, commit}, tmProject) {
    try {
      let response = await tmProjectService.create(tmProject);
      if (response.ok) {
        await dispatch('getAll', true);
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async update ({ dispatch, commit}, tmProject) {
    try {
      let response = await tmProjectService.update(tmProject);
      if (response.ok) {
        await dispatch('getAll', true);
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async delete ({dispatch, commit}, id) {
    let response = await tmProjectService.delete(id);
    if (response.ok) {
      dispatch('getAll', true);
      return true;
    } else {
      return false;
    }
  }
};

export default actions
