import _ from 'lodash';
import configurationResource from "resources/configurationResource";

class Configuration {
  constructor(data) {
    _.merge(this, data);
  }
}

export default {
  async getConfiguration () {
    const response = await configurationResource.query();
    if (response.ok) {
      return new Configuration(response.data);
    } else {
      return null;
    }
  }
}
