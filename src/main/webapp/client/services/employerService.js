import employerResource from '../resources/employerResource';
import _ from 'lodash';
import moment from 'moment';

class Employer {
  constructor(data) {
    _.merge(this, data);
    this.created = data.created ? moment(data.created) : null;
  }

  get fullName() {
    return this.firstName + ' ' + this.surname;
  }
}
export default {
  newEmployer() {
    return new Employer({address: {}});
  },
  async findAll () {
    const response = await employerResource.query();
    if (response.ok) {
      return response.data.map((employerData) => new Employer(employerData));
    } else {
      return null;
    }
  },
  async create (employer) {
    return employerResource.save({}, _.pickBy(employer));
  },
  async update (employer) {
    return employerResource.update({}, _.pickBy(employer));
  },
  async delete(id) {
    return employerResource.delete({id: id});
  }
}
