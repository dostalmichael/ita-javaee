import userResource from '../resources/userResource';
import _ from 'lodash';
import moment from 'moment';

class User {
  constructor(data) {
    _.merge(this, data);
    this.created = moment(data.created);
  }

  get fullName() {
    return this.firstname + ' ' + this.surname;
  }
}

export { User };

export default {
  newUser() {
    return new User({roles: []});
  },
  async me () {
    const response = await userResource.me();
    if (response.ok) {
    return new User(response.data);
    } else {
      return null;
    }
  },
  async updateMe (me) {
    return userResource.updateMe({}, _.pickBy(me));
  },
  async myPasswordChange (credentials) {
    return userResource.myPasswordChange({}, _.pickBy(credentials));
  },
  async passwordChange (userId, credentials) {
    const params = userId ? {userId : userId} : null;
    return userResource.passwordChange(params, credentials);
  },
  async findAll () {
    const response = await userResource.query();
    if (response.ok) {
      return response.data.map((userData) => new User(userData));
    } else {
      return null;
    }
  },
  async create (user) {
    return userResource.save({}, _.pickBy(user));
  },
  async update (user) {
    return userResource.update({}, _.pickBy(user));
  },
  async activate (id) {
    return userResource.activate({id: id}, {});
  },
  async deactivate (id) {
    return userResource.deactivate({id: id}, {});
  },
  async delete(id) {
    return userResource.delete({id: id});
  }
}
