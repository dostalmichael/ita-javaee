import tmProjectResource from '../resources/tmProjectResource';
import _ from 'lodash';
import moment from 'moment';

class TmProject {
  constructor(data) {
    _.merge(this, data);
    this.created = data.created ? moment(data.created) : null;
  }
}
export default {
  newTmProject() {
    return new TmProject({});
  },
  async findAll () {
    const response = await tmProjectResource.query();
    if (response.ok) {
      return response.data.map((tmProjectData) => new TmProject(tmProjectData));
    } else {
      return null;
    }
  },
  async create (tmProject) {
    return tmProjectResource.save({}, _.pickBy(tmProject));
  },
  async update (tmProject) {
    return tmProjectResource.update({}, _.pickBy(tmProject));
  },
  async delete(id) {
    return tmProjectResource.delete({id: id});
  }
}
