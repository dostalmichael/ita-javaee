'use strict';

import _ from 'lodash';

export default (value, codeList) => {
  return _.find(codeList, {id: value}).label;
}
