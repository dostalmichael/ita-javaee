import Vue from 'vue';
import {API_ROOT} from '../config'

export default Vue.resource(API_ROOT + 'employer{/id}', {});
